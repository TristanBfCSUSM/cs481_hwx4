﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Homework_4.Models;
using Homework_4.Views;
using System.IO;
using System.Diagnostics;

namespace Homework_4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]

    public partial class MainPage : ContentPage
    {
        public ObservableCollection<ListProductModels> products { get; set; }

        public MainPage()
        {        
            InitializeComponent();
            products = new ObservableCollection<ListProductModels>();
            Listt.ItemsSource = products;

            //Add pull to refresh
            Listt.RefreshCommand = new Command(() => {

                getListRefresh();
                Listt.IsRefreshing = false;
            });
        }

        //Add a new product
        async void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Views.AddNewProduct{
                BindingContext = new ListProductModels()
            });
        }

        //Refresh the list with saving files
        void getListRefresh()
        {
            var Listproducts = new List<ListProductModels>();

            var newP = Directory.EnumerateFiles(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "*.notes.txt");
            foreach (var filename in newP)
            {
                char separator = '/';
                string text = File.ReadAllText(filename);
                string[] ListSplit = text.Split(separator);
                if (ListSplit.Length > 3)
                {
                    Listproducts.Add(new ListProductModels
                    {
                        filename = filename,
                        productName = ListSplit[0],
                        productDescription = ListSplit[1],
                        productQuantity = ListSplit[2],
                        productImage = ListSplit[3]
                    });
                }
            }
            Listt.ItemsSource = Listproducts;
        }

        //OnAppearing function
        void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            getListRefresh();
        }

        //click on one item and navigate to modify page
        async void Listt_ItemSelected(System.Object sender, Xamarin.Forms.SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new Views.AddNewProduct
                {
                    BindingContext = e.SelectedItem as ListProductModels
                });
            }
        }

        //Click to delete item selected from context action
        async void MenuItem_Clicked_Delete(System.Object sender, System.EventArgs e)
        {
            var product = (sender as MenuItem).BindingContext as ListProductModels;
            if (product != null)
            {
                await DisplayAlert("Delete product", "Your product " + product.productName + " is deleted", "Ok");
                if (File.Exists(product.filename))
                    File.Delete(product.filename);
            }
        }
    }
}
