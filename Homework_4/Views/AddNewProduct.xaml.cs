﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using Homework_4.Models;
using Xamarin.Forms;

namespace Homework_4.Views
{
    public partial class AddNewProduct : ContentPage
    {


        public AddNewProduct()
        {
            InitializeComponent();
        }

        //Click button "Add" to add an item into the list
        async void Button_Clicked_Add(System.Object sender, System.EventArgs e)
        {

            var newList = (ListProductModels)BindingContext;
            string productImageSelected;

            switch (DescriptionP.SelectedItem)
            {
                case "Vegetables":
                    productImageSelected = "vegatables.png";
                    break;
                case "Proteins":
                    productImageSelected = "proteins.png";
                    break;
                case "Fruits":
                    productImageSelected = "fruits.png";
                    break;
                case "Dairy":
                    productImageSelected = "dairy.png";
                    break;
                case "Grains":
                    productImageSelected = "grains.png";
                    break;
                default:
                    productImageSelected = "others.png";
                    break;
            }

            string productText = newList.productName + "/" + DescriptionP.SelectedItem + "/" + newList.productQuantity + '/' + productImageSelected;

            if (string.IsNullOrWhiteSpace(newList.filename))
            {
                string filename = Path.Combine(Environment.GetFolderPath( Environment.SpecialFolder.Personal), $"{newList.productName}.notes.txt");
                File.WriteAllText(filename, productText);
            }
            else
                File.WriteAllText(newList.filename, productText);
            await Navigation.PopAsync();

        }

        //Delete button function
        async void Button_Clicked_Delete(System.Object sender, System.EventArgs e)
        {
            var Product = (ListProductModels)BindingContext;
            if (File.Exists(Product.filename))
                File.Delete(Product.filename);
            await DisplayAlert("Delete product", "Your product is now deleted", "Ok");
            await Navigation.PopAsync();
        }

        //OnAppearing function
        void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            var Product = (ListProductModels)BindingContext;
            if (string.IsNullOrEmpty(Product.productImage))
                ImageDisplay.Source = "addP.png";
            if (!string.IsNullOrEmpty(Product.productName))
                AddButton.Text = "Modify this product";
        }
    }
}
