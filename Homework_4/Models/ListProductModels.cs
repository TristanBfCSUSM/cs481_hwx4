﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Homework_4.Models
{
    public class ListProductModels
    {
        public string productName { get; set; }
        public string productDescription { get; set; }
        public string productQuantity { get; set; } 
        public string productImage { get; set; }
        public string filename { get; set; }

    }
}
